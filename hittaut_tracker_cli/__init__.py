from asyncio import gather, run
from typing import List

from typer import echo, Typer

from hittaut_tracker.database import (
    ActiveRegion,
    database,
    Participant,
    ParticipantRegion,
    Region,
)

app = Typer()


@app.command()
def add_region(id: int, name: str) -> None:
    run(Region.objects.create(id=id, name=name))


@app.command()
def remove_region(id: int) -> None:
    queries = [
        f"DELETE FROM region WHERE id = {id}",
        f"DELETE FROM active_region WHERE region = {id}",
        f"DELETE FROM participant_region WHERE region = {id}",
        f"DELETE FROM hittaut_score WHERE region = {id}",
        f"DELETE FROM leaderboard_score WHERE region = {id}",
    ]

    async def _remove_region():
        for query in queries:
            await database.execute(query=query)

    run(_remove_region())


@app.command()
def add_participant(
    hittaut_alias: str, slack_id: str, tracker_alias: str, regions: List[int]
) -> None:
    async def _add_participant():
        participant = await Participant.objects.create(
            hittaut_alias=hittaut_alias, slack_id=slack_id, tracker_alias=tracker_alias
        )

        async def _add_region(id: int):
            region = await Region.objects.get(id=id)
            await ParticipantRegion.objects.create(
                participant=participant, region=region
            )
            if not await ActiveRegion.objects.filter(region=region).exists():
                await ActiveRegion.objects.create(region=region)

        await gather(*(_add_region(id) for id in regions))

    run(_add_participant())
