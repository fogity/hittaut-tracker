from databases import Database
from orm import Boolean, DateTime, ForeignKey, Integer, Model, String
from sqlalchemy import MetaData

from .settings import settings

database = Database(settings.database_url)
metadata = MetaData()


class Region(Model):
    __database__ = database
    __metadata__ = metadata
    __tablename__ = "region"

    id = Integer(primary_key=True)
    name = String(max_length=50)


class ActiveRegion(Model):
    __database__ = database
    __metadata__ = metadata
    __tablename__ = "active_region"

    id = Integer(primary_key=True)
    region = ForeignKey(Region)


class Participant(Model):
    __database__ = database
    __metadata__ = metadata
    __tablename__ = "participant"

    id = Integer(primary_key=True)
    hittaut_alias = String(max_length=50)
    slack_id = String(max_length=20)
    tracker_alias = String(max_length=50)
    ping_on_slack = Boolean(default=True)
    show_on_leaderboard = Boolean(default=True)


class ParticipantRegion(Model):
    __database__ = database
    __metadata__ = metadata
    __tablename__ = "participant_region"

    id = Integer(primary_key=True)
    participant = ForeignKey(Participant)
    region = ForeignKey(Region)


class HittautScore(Model):
    __database__ = database
    __metadata__ = metadata
    __tablename__ = "hittaut_score"

    id = Integer(primary_key=True)
    hittaut_alias = String(max_length=50)
    region = ForeignKey(Region)
    checkpoints = Integer()
    rank = Integer()
    time = DateTime()


class LeaderboardScore(Model):
    __database__ = database
    __metadata__ = metadata
    __tablename__ = "leaderboard_score"

    id = Integer(primary_key=True)
    participant = ForeignKey(Participant)
    region = ForeignKey(Region)
    checkpoints = Integer()
    rank = Integer()
    time = DateTime()


class GlobalLeaderboardScore(Model):
    __database__ = database
    __metadata__ = metadata
    __tablename__ = "global_leaderboard_score"

    id = Integer(primary_key=True)
    participant = ForeignKey(Participant)
    checkpoints = Integer()
    rank = Integer()
    time = DateTime()
