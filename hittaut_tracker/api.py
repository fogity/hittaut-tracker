from typing import List

from fastapi import APIRouter
from pydantic import BaseModel

from .database import database, LeaderboardScore

router = APIRouter()


class LeaderboardItem(BaseModel):
    tracker_alias: str
    checkpoints: int
    rank: int


@router.get("/leaderboard", response_model=List[LeaderboardItem])
async def leaderboard():
    return await database.fetch_all(
        """
        SELECT
            participant.tracker_alias,
            global_leaderboard_score.checkpoints,
            global_leaderboard_score.rank
        FROM (
            SELECT participant, MAX(id) as id
            FROM global_leaderboard_score
            GROUP BY participant
        ) as latest_score
        JOIN participant
        ON participant.id = latest_score.participant
        JOIN global_leaderboard_score
        ON global_leaderboard_score.id = latest_score.id
        ORDER BY global_leaderboard_score.rank
        """
    )
