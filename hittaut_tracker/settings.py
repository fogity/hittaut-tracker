from pydantic import BaseSettings


class Settings(BaseSettings):
    database_url: str
    crawl_backoff_factor: float
    crawl_backoff_min: float
    crawl_backoff_max_day: float
    crawl_backoff_max_night: float

    class Config:
        env_file = ".env"


settings = Settings()
