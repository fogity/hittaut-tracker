from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from .api import router as api_router
from .crawl import router as crawl_router

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

app.include_router(api_router)
app.include_router(crawl_router)
