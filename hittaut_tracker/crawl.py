from asyncio import CancelledError, create_task, gather, shield, sleep
from asyncio.queues import Queue
from enum import auto, Enum
from itertools import chain, count
from typing import Dict, List, NamedTuple, Tuple

from arrow import now, utcnow
from fastapi import APIRouter
from orm import NoMatch
from ruia import Item, TextField

from .database import (
    ActiveRegion,
    GlobalLeaderboardScore,
    HittautScore,
    LeaderboardScore,
    Participant,
    Region,
)
from .settings import settings

router = APIRouter()

crawl_task = None

crawl_backoff = settings.crawl_backoff_min


class Score(Item):
    target_item = TextField(css_select=".toplist li + li")
    alias = TextField(css_select=".name")
    checkpoints = TextField(css_select=".count")
    rank = TextField(css_select=".rank")

    async def clean_alias(self, value: str) -> str:
        return value.strip()

    async def clean_checkpoints(self, value: str) -> int:
        return int(value)

    async def clean_rank(self, value: str) -> int:
        return int(value)


class ParticipantScore(NamedTuple):
    participant: Participant
    score: Score


class RegionCrawlResult(NamedTuple):
    scores: List[ParticipantScore]
    had_changes: bool


@router.on_event("startup")
def start_crawl() -> None:
    global crawl_task
    crawl_task = create_task(crawl())


@router.on_event("shutdown")
async def stop_crawl() -> None:
    if not crawl_task:
        return
    crawl_task.cancel()
    await crawl_task


async def crawl() -> None:
    global crawl_backoff

    while True:
        current_crawl = crawl_all()

        try:
            had_changes = await shield(current_crawl)
        except CancelledError:
            await current_crawl
            return

        time = now("Europe/Stockholm")
        crawl_backoff = (
            settings.crawl_backoff_min
            if had_changes
            else min(
                2 * crawl_backoff,
                settings.crawl_backoff_max_day
                if 7 < time.hour < 21
                else settings.crawl_backoff_max_night,
            )
        )

        await sleep(crawl_backoff)


async def crawl_all() -> bool:
    results = await gather(
        *(
            crawl_region(ar.region)
            for ar in await ActiveRegion.objects.select_related("region").all()
        )
    )

    combined_scores: Dict[int, Tuple[Participant, int, int]] = {}
    for participant, score in chain(*(r.scores for r in results)):
        if participant.id not in combined_scores:
            combined_scores[participant.id] = (
                participant,
                score.checkpoints,
                score.rank,
            )
            continue
        _, points, rank = combined_scores[participant.id]
        combined_scores[participant.id] = (
            participant,
            points + score.checkpoints,
            min(rank, score.rank),
        )
    for rank, (participant, points, _) in enumerate(
        sorted(combined_scores.values(), key=lambda cs: (-cs[1], cs[2], cs[0].id)), 1
    ):
        if await GlobalLeaderboardScore.objects.filter(
            participant=participant, checkpoints=points, rank=rank,
        ).exists():
            continue
        await GlobalLeaderboardScore.objects.create(
            participant=participant,
            checkpoints=points,
            rank=rank,
            time=utcnow().datetime,
        )

    return any(r.had_changes for r in results)


async def crawl_region(region: Region) -> RegionCrawlResult:
    participant_scores: List[ParticipantScore] = []

    for skip in count(0, 400):
        empty = True

        try:
            async for score in Score.get_items(
                url=f"https://www.orientering.se/user/allusers/?location={region.id}&skip={skip}"
            ):
                empty = False

                try:
                    participant = await Participant.objects.get(
                        hittaut_alias=score.alias
                    )
                    participant_scores.append(ParticipantScore(participant, score))
                except NoMatch:
                    pass

                if await HittautScore.objects.filter(
                    hittaut_alias=score.alias,
                    region=region,
                    checkpoints=score.checkpoints,
                    rank=score.rank,
                ).exists():
                    continue

                await HittautScore.objects.create(
                    hittaut_alias=score.alias,
                    region=region,
                    checkpoints=score.checkpoints,
                    rank=score.rank,
                    time=utcnow().datetime,
                )
        except ValueError:
            break

        if empty:
            break

    return await process_region(region, participant_scores)


async def process_region(
    region: Region, participant_scores: List[ParticipantScore]
) -> RegionCrawlResult:
    participant_scores.sort(key=lambda ps: ps.score.rank)

    has_changed = False

    for rank, ps in enumerate(participant_scores, 1):
        if await LeaderboardScore.objects.filter(
            participant=ps.participant,
            region=region,
            checkpoints=ps.score.checkpoints,
            rank=rank,
        ).exists():
            continue

        has_changed = True

        await LeaderboardScore.objects.create(
            participant=ps.participant,
            region=region,
            checkpoints=ps.score.checkpoints,
            rank=rank,
            time=utcnow().datetime,
        )

    return RegionCrawlResult(participant_scores, has_changed)
