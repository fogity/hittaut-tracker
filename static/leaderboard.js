const donation_per_point = 25;
const donation_maximum = 30000;

function format_donation(donation) {
    return donation.toLocaleString('sv-SE', {
        style: 'currency',
        currency: 'SEK',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });
}

const app = new Vue({
    el: '#app',
    data: {
        leaderboard: [],
        total: {
            points: 0,
            donation: 0,
        },
    },
    methods: {
        fetch_data() {
            axios
                .get('../leaderboard')
                .then(response => {
                    let total_points = 0;
                    let total_donation = 0;
                    response.data.forEach(score => {
                        const donation = donation_per_point * score.checkpoints;
                        score.donation = format_donation(donation);
                        total_points += score.checkpoints;
                        total_donation += donation;
                    });
                    total_donation = Math.min(total_donation, donation_maximum);

                    this.leaderboard = response.data;
                    this.total = {
                        points: total_points,
                        donation: format_donation(total_donation),
                    }
                });
        }
    },
    mounted() {
        this.fetch_data();

        setInterval(function () {
            this.fetch_data();
        }.bind(this), 60000); // 1 minute
    },
});
