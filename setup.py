from setuptools import setup

setup(
    name="hittaut-tracker",
    version="0.0.0",
    packages=["hittaut_tracker", "hittaut_tracker_cli"],
    install_requires=[
        "aiofiles",
        "arrow",
        "databases",
        "fastapi",
        "orm",
        "pydantic[dotenv]",
        "ruia",
        "sqlalchemy",
        "typer",
    ],
    entry_points={"console_scripts": ["hut=hittaut_tracker_cli:app"]},
)
